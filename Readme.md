On cloudera: 

increase heap size of broker

check wich ip address broker is listening, should be not only localhost
Kafka > Instances (Select Instance) > Configuration > Kafka Broker > Advanced > Kafka Broker Advanced Configuration Snippet (Safety Valve) for kafka.properties

Please add (example):

listeners=PLAINTEXT://192.10.0.1:9092,PLAINTEXT://10.0.3.3:9092
listeners=PLAINTEXT://0.0.0.0:9092

in /etc/hosts add
127.0.0.1 quickstart.cloudera and on virtualbox add forward of port 9092

docs: http://kafka-python.readthedocs.io/en/master/apidoc/KafkaConsumer.html 

On hortonworks

change listener if necessary
horton runs in docker. necessary enter docker hosts and add port 6667 in ports to forward.

