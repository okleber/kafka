from kafka import KafkaProducer
from kafka.errors import KafkaError
import datetime
import os, ssl
import logging
logging.basicConfig(level=logging.DEBUG)

'''
# First get all info from the env
ca = os.environ.get('CLOUDKARAFKA_CA')
cert = os.environ.get('CLOUDKARAFKA_CERT')
key = os.environ.get('CLOUDKARAFKA_PRIVATE_KEY')
with open("/tmp/ca.pem", "w") as f:
    f.write(ca)
with open("/tmp/cert.pem", "w") as f:
    f.write(cert)
with open("/tmp/key.pem", "w") as f:
    f.write(key)

ssl_context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)

ssl_context.verify_mode = ssl.CERT_REQUIRED
ssl_context.check_hostname = True
ssl_context.load_verify_locations("/tmp/ca.pem")
ssl_context.load_cert_chain('/tmp/cert.pem', '/tmp/key.pem')

# Load the rest of the env variables
brokers = os.environ.get('CLOUDKARAFKA_BROKERS').split(',')
topic_prefix = os.environ.get('CLOUDKARAFKA_TOPIC_PREFIX')
'''

#brokers = '127.0.0.1' #cloudera port 9092 default
brokers = '127.0.0.1:6667' #hortonworks port 6667 default
topic_prefix = 'kleberpovoacao'
producer = KafkaProducer(bootstrap_servers=brokers)

# Asynchronous by default
count=10000
while True:
    data = str(datetime.datetime.now())+" message number "+str(count)
    data=data.encode('utf-8')
    future = producer.send(topic_prefix,data)
    count-=1
    if count < 1:
        break

    '''
    # Block for 'synchronous' sends
    try:
        record_metadata = future.get(timeout=2)
    except KafkaError:
        pass

    # Successful result returns assigned partition and offset
    print ('topic : {} Partition : {} offset : {}'.format(record_metadata.topic,record_metadata.partition,record_metadata.offset))
    '''
producer.flush()
