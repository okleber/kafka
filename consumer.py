from kafka import KafkaProducer,KafkaConsumer
from kafka.errors import KafkaError
#import logging
#logging.basicConfig(level=logging.DEBUG)

#brokers = '127.0.0.1' #cloudera port 9092 default
brokers = '127.0.0.1:6667' #hortonworks port 6667 default
topic_prefix = 'kleberpovoacao'

consumer = KafkaConsumer(topic_prefix,bootstrap_servers=brokers,auto_offset_reset='earliest')
for msg in consumer:
    print(msg)
